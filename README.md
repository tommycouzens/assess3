This script was created by team DiRT for the third assessment of the
AL DevOps graduate academy. The team was Dilyan, Rahmat and Tommy.
To run the create part of the script for Jenkins and the Vault run the following command
ansible-playbook -i environments/dev start.yml
To destroy everything run:
ansible-playbook -i environments/dev destroy.yml

This will launch the jenkins machine and vault. Once jenkins is up go to it's public Ip:8080, 
then go to the jenkins job called validate_templates and put in a stack name of your choosing. 
Afterwards you will have to wait something like an hour, and then you should be able to go to yourstackname.grads.al-labs.co.uk:8080 and see a working petclinic.




The code is structured thus:
------S3 Bucket---
1. Create an s3 bucket
1.5. If one already exists, skip it.

------VAULT-------
1. Create an instance and put its IP in the hosts file
2. Install EPEL
2. Provision it with Vault from HashiCorp
3. Start and initialise it
4. Unlock it automatically. No manual input needed
5. Create a key called dirt.pem (and remove the old one if one already exists)
6. Put the dirt.pem key in the vault
7. Put admin and password for the databases in the Vault. The default ones are DiRT and dirtysecrets
8. Export the token and vault IP to a file on the local computer

------JENKINS-------
1. Create an instance and put its IP in the hosts file
2. Install EPEL and Java
2. Install Jenkins
3. Provision it with all the software and packages that Jenkins needs
4. Start JENKINS
5. Copy onto the Jenkins machine the IP of Vault, Vault token.
6. Get the jobs onto the server

------PETCLINIC------
The petclinic scripts are launched from the Jenkins server and more specifically from the stable branch 'petclinic' of this git repo
1. Create an instance and pit the IP in the hosts filters
2. Provision it with petclinic
3. Put the petclinic service in init.d to start any time the system starts
4. Create an AMI of the instance and delete the old one if it exists

------Infrastructure------
Go to the jenkins job called validate_templates and put in a stack name. This should run everything from the network to the final application in one touch.
1. Validate templates 
2. Start network and RDS 
3. Wait for RDS to come up
4. Populate the database 
5. Get jar file
6. Use jar file to make petclinic ami
7. Use ami to launch petclinic auto scaling group 
8. curl the petclinic to test if it is working
Then go to stackname.grads.al-labs.co.uk:8080 where stackname is the name you have in the validate_templates step and you should see petclinic!

------Delete Infrastructure------
Run the jenkins job delete_enviroment with the stack name you used to create it.
1. Deletes the petclinic stack and waits for it to finish
2. Deletes everything else






